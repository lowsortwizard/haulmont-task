package com.haulmont.testtask;

import com.vaadin.data.Validator;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

class PhoneNumberValidator implements Validator {
    @Override
    public void validate(Object o) throws InvalidValueException {
        final String content = o.toString();
        if (content.length() == 7) {
            final Pattern pattern = Pattern.compile("^421\\d{4}$");
            final Matcher matcher = pattern.matcher(content);
            if (!matcher.matches()) {
                throw new InvalidValueException("Номер телефона должен начинаться на 421.");
            }
        } else {
            throw new InvalidValueException("Некорректная длина. Номер телефона должен содержать 7 цифр.");
        }
    }
}

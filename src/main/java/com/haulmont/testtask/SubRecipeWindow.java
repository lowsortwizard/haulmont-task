package com.haulmont.testtask;

import com.haulmont.testtask.data.*;
import com.vaadin.data.Validator;
import com.vaadin.data.validator.DateRangeValidator;
import com.vaadin.data.validator.StringLengthValidator;
import com.vaadin.shared.ui.datefield.Resolution;
import com.vaadin.ui.*;
import com.vaadin.ui.themes.ValoTheme;

import java.sql.Date;
import java.time.LocalDate;
import java.time.ZoneId;
import java.util.List;

class SubRecipeWindow extends Window {

    private static final int DEFAULT_ID = 0;
    private static final java.util.Date MIN_DATE = java.util.Date.from(LocalDate.of(2000, 1, 1).atStartOfDay(ZoneId.systemDefault()).toInstant());
    private final DAOFactory factory = DAOFactory.getDaoFactory();
    private final RecipesDAO recipesDAO = factory.getRecipesDAO();

    SubRecipeWindow(Recipe recipe, List<Patient> patients, List<Doctor> doctors) {
        super("Рецепт");
        center();
        setClosable(false);
        setResizable(false);

        final VerticalLayout content = new VerticalLayout();
        content.setMargin(true);
        content.setSpacing(true);

        final TextField description = new TextField("Описание");
        description.addValidator(new StringLengthValidator("Длина должна быть в диапазоне 1-255 символа.", 1, 255, false));

        final NativeSelect patientsList = new NativeSelect("Пациент");
        patientsList.setNullSelectionAllowed(false);
        patientsList.addItems(patients);

        final NativeSelect doctorsList = new NativeSelect("Доктор");
        doctorsList.setNullSelectionAllowed(false);
        doctorsList.addItems(doctors);

        final PopupDateField date = new PopupDateField("Дата создания");
        date.addValidator(new DateRangeValidator("Некорректная дата", MIN_DATE, null, Resolution.DAY));

        final NativeSelect priority = new NativeSelect("Приоритет");
        priority.setNullSelectionAllowed(false);
        priority.addItems(Priority.values());

        final Button okButton = new Button("Ок");
        okButton.setStyleName(ValoTheme.BUTTON_PRIMARY);

        final Button cancelButton = new Button("Отменить");

        final HorizontalLayout buttons =
                new HorizontalLayout(okButton, cancelButton);
        buttons.setSpacing(true);

        content.addComponents(description, patientsList, doctorsList, date, priority, buttons);

        description.setValue(recipe.getDescription());
        patientsList.setValue(recipe.getPatient());
        doctorsList.setValue(recipe.getDoctor());
        date.setValue(recipe.getCreationDate());
        priority.setValue(recipe.getPriority());

        setContent(content);

        okButton.addClickListener(click -> {
            try {
                description.validate();
                date.validate();

                recipe.setDescription(description.getValue());
                recipe.setPatient((Patient) patientsList.getValue());
                recipe.setDoctor((Doctor) doctorsList.getValue());
                LocalDate convertedDate =
                        date.getValue().toInstant().atZone(ZoneId.systemDefault()).toLocalDate();
                recipe.setCreationDate(Date.valueOf(convertedDate));
                recipe.setPriority((Priority) priority.getValue());

                if (recipe.getId() == DEFAULT_ID) {
                    recipesDAO.insert(recipe);
                } else recipesDAO.update(recipe);

                close();
            } catch (Validator.InvalidValueException e) {
                Notification.show("Проверьте корректность полей.");
            }
        });

        cancelButton.addClickListener(clickEvent -> close());
    }
}

package com.haulmont.testtask;

import com.haulmont.testtask.data.DAOFactory;
import com.haulmont.testtask.data.Doctor;
import com.haulmont.testtask.data.DoctorsDAO;
import com.vaadin.data.Validator;
import com.vaadin.ui.*;
import com.vaadin.ui.themes.ValoTheme;

class SubDoctorWindow extends Window {

    private static final int DEFAULT_ID = 0;
    private final DAOFactory factory = DAOFactory.getDaoFactory();
    private final DoctorsDAO doctorsDAO = factory.getDoctorsDAO();

    SubDoctorWindow(Doctor doctor) {
        super("Врач");
        center();
        setClosable(false);
        setResizable(false);

        final VerticalLayout content = new VerticalLayout();
        content.setMargin(true);
        content.setSpacing(true);

        final TextField name = new TextField("Имя");
        name.addValidator(new TextFieldValidator(50));

        final TextField surname = new TextField("Фамилиия");
        surname.addValidator(new TextFieldValidator(50));

        final TextField patronymic = new TextField("Отчество");
        patronymic.addValidator(new TextFieldValidator(50));

        final TextField specialization = new TextField("Специализация");
        specialization.addValidator(new TextFieldValidator(50));

        final Button okButton = new Button("Ок");
        okButton.setStyleName(ValoTheme.BUTTON_PRIMARY);

        final Button cancelButton = new Button("Отменить");

        final HorizontalLayout buttons =
                new HorizontalLayout(okButton, cancelButton);
        buttons.setSpacing(true);

        content.addComponents(name, surname, patronymic,
                specialization, buttons);

        name.setValue(doctor.getName());
        surname.setValue(doctor.getSurname());
        patronymic.setValue(doctor.getPatronymic());
        specialization.setValue(doctor.getSpecialization());

        setContent(content);

        okButton.addClickListener(click -> {
            try {
                name.validate();
                surname.validate();
                patronymic.validate();
                specialization.validate();

                doctor.setName(name.getValue());
                doctor.setSurname(surname.getValue());
                doctor.setPatronymic(patronymic.getValue());
                doctor.setSpecialization(specialization.getValue());

                if (doctor.getId() == DEFAULT_ID) {
                    doctorsDAO.insert(doctor);
                } else doctorsDAO.update(doctor);

                close();
            } catch (Validator.InvalidValueException e) {
                Notification.show("Проверьте корректность полей.");
            }
        });

        cancelButton.addClickListener(clickEvent -> close());
    }
}

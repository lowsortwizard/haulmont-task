package com.haulmont.testtask;

import com.haulmont.testtask.data.*;
import com.vaadin.annotations.Theme;
import com.vaadin.data.util.BeanItemContainer;
import com.vaadin.server.FontAwesome;
import com.vaadin.server.Page;
import com.vaadin.server.VaadinRequest;
import com.vaadin.ui.*;
import com.vaadin.ui.themes.ValoTheme;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

@Theme(ValoTheme.THEME_NAME)
public class MainUI extends UI {

    private static final String ADD_BUTTON_NAME = "Добавить";
    private static final String EDIT_BUTTON_NAME = "Изменить";
    private static final String REMOVE_BUTTON_NAME = "Удалить";

    private DoctorsDAO doctorsDAO;
    private PatientsDAO patientsDAO;
    private RecipesDAO recipesDAO;

    private Grid doctorsGrid;
    private Grid patientsGrid;
    private Grid recipesGrid;

    private List<Doctor> doctors;
    private List<Patient> patients;
    private List<Recipe> recipes;

    private SubPatientWindow subPatientWindow;
    private SubDoctorWindow subDoctorWindow;
    private SubRecipeWindow subRecipeWindow;

    private Button addPatientButton;
    private Button editPatientButton;
    private Button removePatientButton;

    private Button addDoctorButton;
    private Button editDoctorButton;
    private Button removeDoctorButton;
    private Button showStatisticButton;

    private Button addRecipeButton;
    private Button editRecipeButton;
    private Button removeRecipeButton;
    private Button applyFilterButton;
    private Button clearFilterButton;

    public MainUI() {
        final DAOFactory factory = DAOFactory.getDaoFactory();
        doctorsDAO = factory.getDoctorsDAO();
        patientsDAO = factory.getPatientsDAO();
        recipesDAO = factory.getRecipesDAO();

        doctorsGrid = new Grid();
        patientsGrid = new Grid();
        recipesGrid = new Grid();

        addPatientButton = new Button(ADD_BUTTON_NAME);
        editPatientButton = new Button(EDIT_BUTTON_NAME);
        removePatientButton = new Button(REMOVE_BUTTON_NAME);

        addDoctorButton = new Button(ADD_BUTTON_NAME);
        editDoctorButton = new Button(EDIT_BUTTON_NAME);
        removeDoctorButton = new Button(REMOVE_BUTTON_NAME);
        showStatisticButton = new Button("Показать статистику");

        addRecipeButton = new Button(ADD_BUTTON_NAME);
        editRecipeButton = new Button(EDIT_BUTTON_NAME);
        removeRecipeButton = new Button(REMOVE_BUTTON_NAME);
        applyFilterButton = new Button("Применить");
        clearFilterButton = new Button(FontAwesome.TIMES);
    }

    @Override
    protected void init(VaadinRequest request) {
        final VerticalLayout layout = new VerticalLayout();
        layout.setSpacing(true);
        layout.setMargin(true);

        patientsGrid.setSizeFull();
        patientsGrid.setColumns("surname", "name", "patronymic", "phone");
        updatePatientsList();

        final HorizontalLayout patientButtonsLayout =
                new HorizontalLayout(addPatientButton, editPatientButton, removePatientButton);
        patientButtonsLayout.setSpacing(true);

        final VerticalLayout patientComponentLayout =
                new VerticalLayout(patientsGrid, patientButtonsLayout);
        patientComponentLayout.setSpacing(true);
        patientComponentLayout.setComponentAlignment(patientButtonsLayout, Alignment.BOTTOM_RIGHT);

        addPatientButton.addClickListener(click -> {
            patientsGrid.deselectAll();
            changeButtonsState(false);

            subPatientWindow = new SubPatientWindow(new Patient());
            addWindow(subPatientWindow);
            subPatientWindow.addCloseListener(e -> {
                changeButtonsState(true);
                updatePatientsList();
                subPatientWindow = null;
            });
        });

        editPatientButton.addClickListener(click -> {
            changeButtonsState(false);

            final Object selectedRow = patientsGrid.getSelectedRow();
            if (selectedRow != null) {
                patientsGrid.deselectAll();
                subPatientWindow = new SubPatientWindow((Patient) selectedRow);
                addWindow(subPatientWindow);
                subPatientWindow.addCloseListener(e -> {
                    changeButtonsState(true);
                    updatePatientsList();
                    subPatientWindow = null;
                });
            } else {
                Notification.show("Пожалуйста, выделите строку.");
                changeButtonsState(true);
            }
        });

        removePatientButton.addClickListener(click -> {
            final Object selectedRow = patientsGrid.getSelectedRow();
            if (selectedRow != null) {
                patientsGrid.deselectAll();
                try {
                    patientsDAO.remove((Patient) selectedRow);
                    updatePatientsList();
                } catch (SQLException e) {
                    Notification.show("Невозможно удалить пациента, который связан с рецептом.");
                }
            } else {
                Notification.show("Пожалуйста, выделите строку.");
            }
        });

        doctorsGrid.setSizeFull();
        doctorsGrid.setColumns("surname", "name", "patronymic", "specialization");
        updateDoctorsList();

        final HorizontalLayout doctorButtonsLayout =
                new HorizontalLayout(addDoctorButton, editDoctorButton, removeDoctorButton, showStatisticButton);
        doctorButtonsLayout.setSpacing(true);

        final VerticalLayout doctorComponentLayout =
                new VerticalLayout(doctorsGrid, doctorButtonsLayout);
        doctorComponentLayout.setSpacing(true);
        doctorComponentLayout.setComponentAlignment(doctorButtonsLayout, Alignment.BOTTOM_RIGHT);

        addDoctorButton.addClickListener(click -> {
            doctorsGrid.deselectAll();
            changeButtonsState(false);

            subDoctorWindow = new SubDoctorWindow(new Doctor());
            addWindow(subDoctorWindow);
            subDoctorWindow.addCloseListener(e -> {
                changeButtonsState(true);
                updateDoctorsList();
                subDoctorWindow = null;
            });
        });

        editDoctorButton.addClickListener(click -> {
            changeButtonsState(false);

            final Object selectedRow = doctorsGrid.getSelectedRow();
            if (selectedRow != null) {
                doctorsGrid.deselectAll();
                subDoctorWindow = new SubDoctorWindow((Doctor) selectedRow);
                addWindow(subDoctorWindow);
                subDoctorWindow.addCloseListener(e -> {
                    changeButtonsState(true);
                    updateDoctorsList();
                    subDoctorWindow = null;
                });
            } else {
                Notification.show("Пожалуйста, выделите строку.");
                changeButtonsState(true);
            }
        });

        removeDoctorButton.addClickListener(click -> {
            final Object selectedRow = doctorsGrid.getSelectedRow();
            if (selectedRow != null) {
                doctorsGrid.deselectAll();
                try {
                    doctorsDAO.remove((Doctor) selectedRow);
                    updateDoctorsList();
                } catch (SQLException e) {
                    Notification.show("Невозможно удалить доктора, который связан с рецептом.");
                }
            } else {
                Notification.show("Пожалуйста, выделите строку.");
            }
        });

        showStatisticButton.addClickListener(click -> {
            final Object selectedRow = doctorsGrid.getSelectedRow();
            if (selectedRow != null) {
                doctorsGrid.deselectAll();
                int quantity = doctorsDAO.getRecipesQuantity((Doctor) selectedRow);
                Notification statisticNotification =
                        new Notification(String.format("Прописано рецептов: %d доктором: %s.", quantity, selectedRow.toString()), Notification.Type.TRAY_NOTIFICATION);
                statisticNotification.show(Page.getCurrent());
            } else {
                Notification.show("Пожалуйста, выделите строку.");
            }
        });

        recipesGrid.setSizeFull();
        recipesGrid.setColumns("description", "patient", "doctor", "creationDate", "priority");
        updateRecipesList();

        final NativeSelect patientFilter = new NativeSelect();
        patientFilter.addItems(patients);
        patientFilter.setNullSelectionAllowed(false);
        patientFilter.setValue(patients.get(0));

        final NativeSelect priorityFilter = new NativeSelect();
        priorityFilter.addItems(Priority.values());
        priorityFilter.setNullSelectionAllowed(false);
        priorityFilter.setValue(Priority.NORMAL);

        final TextField descriptionFilter = new TextField();

        final NativeSelect filterType = new NativeSelect();
        filterType.addItems(FilterTypes.values());
        filterType.setItemCaption(FilterTypes.PATIENT, "Пациент");
        filterType.setItemCaption(FilterTypes.PRIORITY, "Приоритет");
        filterType.setItemCaption(FilterTypes.DESCRIPTION, "Описание");
        filterType.setNullSelectionAllowed(false);
        filterType.setValue(FilterTypes.PATIENT);

        final HorizontalLayout filterLayout = new HorizontalLayout(filterType, patientFilter, applyFilterButton, clearFilterButton);
        filterLayout.setSpacing(true);

        final HorizontalLayout recipeButtonsLayout =
                new HorizontalLayout(addRecipeButton, editRecipeButton, removeRecipeButton);
        recipeButtonsLayout.setSpacing(true);

        final VerticalLayout recipeComponentLayout =
                new VerticalLayout(recipesGrid, filterLayout, recipeButtonsLayout);
        recipeComponentLayout.setSpacing(true);
        recipeComponentLayout.setComponentAlignment(recipeButtonsLayout, Alignment.BOTTOM_RIGHT);

        addRecipeButton.addClickListener(click -> {
            recipesGrid.deselectAll();
            changeButtonsState(false);
            subRecipeWindow = new SubRecipeWindow(new Recipe(patients.get(0), doctors.get(0)), patients, doctors);
            addWindow(subRecipeWindow);

            subRecipeWindow.addCloseListener(e -> {
                changeButtonsState(true);
                updateRecipesList();
                subRecipeWindow = null;
            });
        });

        editRecipeButton.addClickListener(click -> {
            changeButtonsState(false);

            final Object selectedRow = recipesGrid.getSelectedRow();
            if (selectedRow != null) {
                recipesGrid.deselectAll();
                subRecipeWindow = new SubRecipeWindow((Recipe) selectedRow, patients, doctors);
                addWindow(subRecipeWindow);
                subRecipeWindow.addCloseListener(e -> {
                    changeButtonsState(true);
                    updateRecipesList();
                    subRecipeWindow = null;
                });
            } else {
                Notification.show("Пожалуйста, выделите строку.");
                changeButtonsState(true);
            }
        });

        removeRecipeButton.addClickListener(click -> {
            final Object selectedRow = recipesGrid.getSelectedRow();
            if (selectedRow != null) {
                recipesGrid.deselectAll();
                recipesDAO.remove((Recipe) selectedRow);
                updateRecipesList();
            } else {
                Notification.show("Пожалуйста, выделите строку.");
            }
        });

        filterType.addValueChangeListener(e -> {
            FilterTypes type = (FilterTypes) filterType.getValue();
            switch (type) {
                case PRIORITY:
                    filterLayout.removeComponent(patientFilter);
                    filterLayout.removeComponent(descriptionFilter);
                    filterLayout.addComponent(priorityFilter, 1);
                    break;
                case DESCRIPTION:
                    filterLayout.removeComponent(patientFilter);
                    filterLayout.removeComponent(priorityFilter);
                    filterLayout.addComponent(descriptionFilter, 1);
                    break;
                default:
                    filterLayout.removeComponent(descriptionFilter);
                    filterLayout.removeComponent(priorityFilter);
                    filterLayout.addComponent(patientFilter, 1);
                    break;
            }
        });

        applyFilterButton.addClickListener(click -> {
            FilterTypes type = (FilterTypes) filterType.getValue();
            List<Recipe> filteredRecipes = new ArrayList<>();
            switch (type) {
                case PRIORITY:
                    Priority priority = (Priority) priorityFilter.getValue();
                    Priority thisPriority;
                    for (Recipe recipe : recipes) {
                        thisPriority = recipe.getPriority();
                        if (priority.hashCode() == thisPriority.hashCode() && priority.equals(thisPriority)) {
                            filteredRecipes.add(recipe);
                        }
                    }
                    updateRecipesList(filteredRecipes);
                    break;
                case DESCRIPTION:
                    String description = descriptionFilter.getValue();
                    for (Recipe recipe : recipes) {
                        if (recipe.getDescription().contains(description)) {
                            filteredRecipes.add(recipe);
                        }
                    }
                    updateRecipesList(filteredRecipes);
                    break;
                default:
                    Patient patient = (Patient) patientFilter.getValue();
                    Patient thisPatient;
                    for (Recipe recipe : recipes) {
                        thisPatient = recipe.getPatient();
                        if (patient.hashCode() == thisPatient.hashCode() && patient.equals(thisPatient)) {
                            filteredRecipes.add(recipe);
                        }
                    }
                    updateRecipesList(filteredRecipes);
                    break;
            }
        });

        clearFilterButton.addClickListener(click -> updateRecipesList());

        layout.addComponents(patientComponentLayout, doctorComponentLayout, recipeComponentLayout);
        setContent(layout);
    }


    private void updatePatientsList() {
        patients = patientsDAO.getAll();
        patientsGrid
                .setContainerDataSource(new BeanItemContainer<>(Patient.class, patients));
    }

    private void updateDoctorsList() {
        doctors = doctorsDAO.getAll();
        doctorsGrid
                .setContainerDataSource(new BeanItemContainer<>(Doctor.class, doctors));
    }

    private void updateRecipesList() {
        recipes = recipesDAO.getAll();
        updateRecipesList(recipes);
    }

    private void updateRecipesList(List<Recipe> recipes) {
        recipesGrid
                .setContainerDataSource(new BeanItemContainer<>(Recipe.class, recipes));
    }

    private void changeButtonsState(boolean state) {
        addPatientButton.setEnabled(state);
        editPatientButton.setEnabled(state);
        removePatientButton.setEnabled(state);

        addDoctorButton.setEnabled(state);
        editDoctorButton.setEnabled(state);
        removeDoctorButton.setEnabled(state);
        showStatisticButton.setEnabled(state);

        addRecipeButton.setEnabled(state);
        editRecipeButton.setEnabled(state);
        removeRecipeButton.setEnabled(state);
        applyFilterButton.setEnabled(state);
        clearFilterButton.setEnabled(state);
    }
}
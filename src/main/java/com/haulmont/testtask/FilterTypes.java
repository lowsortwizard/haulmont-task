package com.haulmont.testtask;

public enum FilterTypes {
    PATIENT,
    PRIORITY,
    DESCRIPTION
}

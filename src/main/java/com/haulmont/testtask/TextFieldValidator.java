package com.haulmont.testtask;

import com.vaadin.data.Validator;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class TextFieldValidator implements Validator {

    private int maxLength;

    TextFieldValidator(int maxLength) {
        this.maxLength = maxLength;
    }

    @Override
    public void validate(Object o) throws InvalidValueException {
        final String content = o.toString();
        if (content.length() >= 2 && content.length() <= maxLength) {
            final Pattern pattern = Pattern.compile("^[А-яЁё]+$");
            final Matcher matcher = pattern.matcher(content);
            if (!matcher.matches()) {
                throw new InvalidValueException("Поле может содержать только буквы русского алфавита.");
            }
        } else {
            throw new InvalidValueException(String.format("Некорректная длина. Длина должна быть в диапазоне 2-%d символов.", maxLength));
        }
    }
}

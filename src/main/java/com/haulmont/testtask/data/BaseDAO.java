package com.haulmont.testtask.data;

import java.sql.SQLException;
import java.util.List;

public interface BaseDAO<T> {

    int insert(T object);

    List<T> getAll();

    void update(T object);

    void remove(T object) throws SQLException;
}

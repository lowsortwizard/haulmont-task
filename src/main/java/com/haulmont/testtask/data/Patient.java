package com.haulmont.testtask.data;

import java.util.Objects;

public class Patient extends Human {

    private String phone;

    public Patient() {
        this("", "", "", "");
    }

    public Patient(String name, String surname, String patronymic, String phone) {
        super(name, surname, patronymic);
        this.phone = phone;
    }

    protected Patient(long id, String name, String surname, String patronymic, String phone) {
        super(id, name, surname, patronymic);
        this.phone = phone;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    @Override
    public String toString() {
        return super.toString();
    }

    @Override
    public boolean equals(Object o) {
        if (!super.equals(o)) return false;
        Patient patient = (Patient) o;
        return phone.equalsIgnoreCase(patient.phone);
    }

    @Override
    public int hashCode() {
        return Objects.hash(super.hashCode(), phone);
    }
}

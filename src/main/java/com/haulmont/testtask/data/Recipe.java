package com.haulmont.testtask.data;

import java.sql.Date;
import java.time.LocalDate;
import java.util.Objects;

public class Recipe {

    private long id;
    private String description;
    private Patient patient;
    private Doctor doctor;
    private Date creationDate;
    private Priority priority;

    public Recipe(Patient patient, Doctor doctor) {
        this("", patient, doctor, Date.valueOf(LocalDate.now()), Priority.NORMAL);
    }

    protected Recipe(long id, String description, Patient patient, Doctor doctor, Date creationDate, Priority priority) {
        this(description, patient, doctor, creationDate, priority);
        this.id = id;
    }

    public Recipe(String description, Patient patient, Doctor doctor, Date creationDate, Priority priority) {
        this.description = description;
        this.patient = patient;
        this.doctor = doctor;
        this.creationDate = creationDate;
        this.priority = priority;
    }

    public long getId() {
        return id;
    }

    protected void setId(long id) {
        this.id = id;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Patient getPatient() {
        return patient;
    }

    public long getPatientId() {
        return patient.getId();
    }

    public void setPatient(Patient patient) {
        this.patient = patient;
    }

    public Doctor getDoctor() {
        return doctor;
    }

    public void setDoctor(Doctor doctor) {
        this.doctor = doctor;
    }

    public long getDoctorId() {
        return doctor.getId();
    }

    public Date getCreationDate() {
        return creationDate;
    }

    public void setCreationDate(Date creationDate) {
        this.creationDate = creationDate;
    }

    public Priority getPriority() {
        return priority;
    }

    public void setPriority(Priority priority) {
        this.priority = priority;
    }

    public long getPriorityIndex() {
        return priority.getIndex();
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Recipe recipe = (Recipe) o;
        return id == recipe.id &&
                Objects.equals(description, recipe.description) &&
                Objects.equals(patient, recipe.patient) &&
                Objects.equals(doctor, recipe.doctor) &&
                Objects.equals(creationDate, recipe.creationDate) &&
                priority == recipe.priority;
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, description, patient, doctor, creationDate, priority);
    }

}

package com.haulmont.testtask.data;

import java.sql.SQLException;
import java.util.List;

public interface RecipesDAO extends BaseDAO<Recipe> {

    int insert(Recipe recipe);

    List<Recipe> getAll();

    void update(Recipe recipe);

    void remove(Recipe recipe);
}

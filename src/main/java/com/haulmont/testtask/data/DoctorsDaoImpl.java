package com.haulmont.testtask.data;

import static com.haulmont.testtask.db.DatabaseHelper.getConnection;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

public class DoctorsDaoImpl implements DoctorsDAO {

    @Override
    public int insert(Doctor doctor) {
        String sql = "INSERT INTO DOCTORS(NAME, SURNAME, PATRONYMIC, SPECIALIZATION) VALUES (?, ?, ?, ?)";
        try (Connection connection = getConnection();
             PreparedStatement statement = Objects.requireNonNull(connection).prepareStatement(sql)) {
            statement.setString(1, doctor.getName());
            statement.setString(2, doctor.getSurname());
            statement.setString(3, doctor.getPatronymic());
            statement.setString(4, doctor.getSpecialization());
            return statement.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return 0;
    }

    @Override
    public List<Doctor> getAll() {
        final List<Doctor> doctors = new ArrayList<>();
        String sql = "SELECT * FROM DOCTORS";
        try (Connection connection = getConnection();
             Statement statement = Objects.requireNonNull(connection).createStatement()) {
            try (ResultSet rs = statement.executeQuery(sql)) {
                while (rs.next()) {
                    doctors.add(new Doctor(rs.getLong(1),
                            rs.getString(2),
                            rs.getString(3),
                            rs.getString(4),
                            rs.getString(5)));
                }
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return doctors;
    }

    @Override
    public void update(Doctor doctor) {
        String sql = "UPDATE DOCTORS SET NAME =?, SURNAME = ?, PATRONYMIC = ?, SPECIALIZATION = ?  WHERE ID = ?";
        try (Connection connection = getConnection();
             PreparedStatement statement = Objects.requireNonNull(connection).prepareStatement(sql)) {
            statement.setString(1, doctor.getName());
            statement.setString(2, doctor.getSurname());
            statement.setString(3, doctor.getPatronymic());
            statement.setString(4, doctor.getSpecialization());
            statement.setLong(5, doctor.getId());
            statement.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void remove(Doctor doctor) throws SQLException {
        String sql = "DELETE FROM DOCTORS WHERE ID=?";
        try (Connection connection = getConnection();
             PreparedStatement statement = Objects.requireNonNull(connection).prepareStatement(sql)) {
            statement.setLong(1, doctor.getId());
            statement.executeUpdate();
        }
    }

    @Override
    public int getRecipesQuantity(Doctor doctor) {
        String sql = "SELECT COUNT(*) FROM RECIPES WHERE DOCTOR_ID = ?";
        try (Connection connection = getConnection();
             PreparedStatement statement = Objects.requireNonNull(connection).prepareStatement(sql)) {
            statement.setLong(1, doctor.getId());
            ResultSet resultSet = statement.executeQuery();
            if (resultSet.next()) {
                return resultSet.getInt(1);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return 0;
    }
}

package com.haulmont.testtask.data;

import java.sql.SQLException;
import java.util.List;

public interface PatientsDAO extends BaseDAO<Patient> {

    int insert(Patient patient);

    List<Patient> getAll();

    void update(Patient patient);

    void remove(Patient patient) throws SQLException;
}

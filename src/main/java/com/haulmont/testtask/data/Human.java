package com.haulmont.testtask.data;

import java.util.Objects;

public abstract class Human {

    private long id;
    private String name;
    private String surname;
    private String patronymic;

    protected Human(long id, String name, String surname, String patronymic) {
        this(name, surname, patronymic);
        this.id = id;
    }

    public Human(String name, String surname, String patronymic) {
        this.name = name;
        this.surname = surname;
        this.patronymic = patronymic;
    }

    public long getId() {
        return id;
    }

    protected void setId(long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSurname() {
        return surname;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }

    public String getPatronymic() {
        return patronymic;
    }

    public void setPatronymic(String patronymic) {
        this.patronymic = patronymic;
    }


    @Override
    public String toString() {
        return String.format("%s %s %s", surname, name, patronymic);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Human human = (Human) o;
        return name.equalsIgnoreCase(human.name) &&
                surname.equalsIgnoreCase(human.surname) &&
                patronymic.equalsIgnoreCase(human.patronymic);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, name, surname, patronymic);
    }
}

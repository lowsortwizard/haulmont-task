package com.haulmont.testtask.data;

import java.util.Objects;

public class Doctor extends Human {

    private String specialization;

    public Doctor() {
        this("", "", "", "");
    }

    protected Doctor(long id, String name, String surname, String patronymic, String specialization) {
        super(id, name, surname, patronymic);
        this.specialization = specialization;
    }

    public Doctor(String name, String surname, String patronymic, String specialization) {
        super(name, surname, patronymic);
        this.specialization = specialization;
    }

    public String getSpecialization() {
        return specialization;
    }

    public void setSpecialization(String specialization) {
        this.specialization = specialization;
    }

    @Override
    public String toString() {
        return super.toString();
    }

    @Override
    public boolean equals(Object o) {
        if (!super.equals(o)) return false;
        Doctor doctor = (Doctor) o;
        return specialization.equalsIgnoreCase(doctor.specialization);
    }

    @Override
    public int hashCode() {
        return Objects.hash(super.hashCode(), specialization);
    }
}

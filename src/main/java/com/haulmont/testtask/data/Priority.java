package com.haulmont.testtask.data;

public enum Priority {
    NORMAL(0),
    CITO(1),
    STATIM(2);

    private final long index;

    Priority(long index) {
        this.index = index;
    }

    public long getIndex() {
        return index;
    }
}

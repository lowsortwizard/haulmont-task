package com.haulmont.testtask.data;

import java.sql.SQLException;
import java.util.List;

public interface DoctorsDAO extends BaseDAO<Doctor> {

    int insert(Doctor doctor);

    List<Doctor> getAll();

    void update(Doctor doctor);

    void remove(Doctor doctor) throws SQLException;

    int getRecipesQuantity(Doctor doctor);
}

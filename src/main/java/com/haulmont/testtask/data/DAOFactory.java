package com.haulmont.testtask.data;

public abstract class DAOFactory {

    public static DAOFactory getDaoFactory() {
        return new HSQLPerRequestDAOFactory();
    }

    public abstract DoctorsDAO getDoctorsDAO();
    public abstract PatientsDAO getPatientsDAO();
    public abstract RecipesDAO getRecipesDAO();
}

package com.haulmont.testtask.data;

public class HSQLPerRequestDAOFactory extends DAOFactory {

    @Override
    public DoctorsDAO getDoctorsDAO() {
        return new DoctorsDaoImpl();
    }

    @Override
    public PatientsDAO getPatientsDAO() {
        return new PatientsDaoImpl();
    }

    @Override
    public RecipesDAO getRecipesDAO() {
        return new RecipesDaoImpl();
    }

}

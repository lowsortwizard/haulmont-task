package com.haulmont.testtask.data;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

import static com.haulmont.testtask.db.DatabaseHelper.getConnection;

public class RecipesDaoImpl implements RecipesDAO {

    @Override
    public int insert(Recipe recipe) {
        String sql = "INSERT INTO RECIPES(description, patient_id, doctor_id, date, priority) VALUES (?, ?, ?,?, ?)";
        try (Connection connection = getConnection();
             PreparedStatement statement = Objects.requireNonNull(connection).prepareStatement(sql)) {
            statement.setString(1, recipe.getDescription());
            statement.setLong(2, recipe.getPatientId());
            statement.setLong(3, recipe.getDoctorId());
            statement.setDate(4, recipe.getCreationDate());
            statement.setLong(5, recipe.getPriorityIndex());
            return statement.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return 0;
    }

    @Override
    public List<Recipe> getAll() {
        final List<Recipe> recipes = new ArrayList<>();
        String sql = "SELECT R.ID, R.DESCRIPTION," +
                "P.ID, P.NAME, P.SURNAME, P.PATRONYMIC, P.PHONE_NUMBER," +
                "D.ID, D.NAME, D.SURNAME, D.PATRONYMIC, D.SPECIALIZATION, " +
                "R.DATE, P2.NAME " +
                "FROM RECIPES R " +
                "INNER JOIN PATIENTS P on R.PATIENT_ID = P.ID " +
                "INNER JOIN DOCTORS D on R.DOCTOR_ID = D.ID " +
                "INNER JOIN PRIORITY P2 on R.PRIORITY = P2.ID";
        try (Connection connection = getConnection();
             Statement statement = Objects.requireNonNull(connection).createStatement()) {
            try (ResultSet rs = statement.executeQuery(sql)) {
                Patient patient;
                Doctor doctor;
                while (rs.next()) {
                    patient = new Patient(rs.getLong(3), rs.getString(4), rs.getString(5),
                            rs.getString(6), rs.getString(7));

                    doctor = new Doctor(rs.getLong(8), rs.getString(9), rs.getString(10),
                            rs.getString(11), rs.getString(12));

                    recipes.add(new Recipe(rs.getLong(1), rs.getString(2), patient, doctor,
                            rs.getDate(13), Priority.valueOf(rs.getString(14).toUpperCase())));
                }
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return recipes;
    }

    @Override
    public void update(Recipe recipe) {
        String sql = "UPDATE RECIPES SET DESCRIPTION = ?, PATIENT_ID = ?, DOCTOR_ID = ?, DATE = ?, PRIORITY = ? WHERE ID = ?";
        try (Connection connection = getConnection();
             PreparedStatement statement = Objects.requireNonNull(connection).prepareStatement(sql)) {
            statement.setString(1, recipe.getDescription());
            statement.setLong(2, recipe.getPatientId());
            statement.setLong(3, recipe.getDoctorId());
            statement.setDate(4, recipe.getCreationDate());
            statement.setLong(5, recipe.getPriorityIndex());
            statement.setLong(6, recipe.getId());
            statement.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void remove(Recipe recipe)  {
        String sql = "DELETE FROM RECIPES WHERE ID=?";
        try {
            try (Connection connection = getConnection();
                 PreparedStatement statement = Objects.requireNonNull(connection).prepareStatement(sql)) {
                statement.setLong(1, recipe.getId());
                statement.executeUpdate();
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }
}

package com.haulmont.testtask.data;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

import static com.haulmont.testtask.db.DatabaseHelper.getConnection;

public class PatientsDaoImpl implements PatientsDAO {

    @Override
    public int insert(Patient patient) {
        String sql = "INSERT INTO PATIENTS(name, surname, patronymic, PHONE_NUMBER) VALUES (?, ?, ?, ?)";
        try (Connection connection = getConnection();
             PreparedStatement statement = Objects.requireNonNull(connection).prepareStatement(sql)) {
            statement.setString(1, patient.getName());
            statement.setString(2, patient.getSurname());
            statement.setString(3, patient.getPatronymic());
            statement.setString(4, patient.getPhone());
            return statement.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return 0;
    }

    @Override
    public List<Patient> getAll() {
        final List<Patient> patients = new ArrayList<>();
        String sql = "SELECT * FROM PATIENTS";
        try (Connection connection = getConnection();
             Statement statement = Objects.requireNonNull(connection).createStatement()) {
            try (ResultSet rs = statement.executeQuery(sql)) {
                while (rs.next()) {
                    patients.add(new Patient(rs.getLong(1),
                            rs.getString(2),
                            rs.getString(3),
                            rs.getString(4),
                            rs.getString(5)));
                }
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return patients;
    }

    @Override
    public void update(Patient patient) {
        String sql = "UPDATE PATIENTS SET NAME =?, SURNAME = ?, PATRONYMIC = ?, PHONE_NUMBER = ? WHERE ID = ?";
        try (Connection connection = getConnection();
             PreparedStatement statement = Objects.requireNonNull(connection).prepareStatement(sql)) {
            statement.setString(1, patient.getName());
            statement.setString(2, patient.getSurname());
            statement.setString(3, patient.getPatronymic());
            statement.setString(4, patient.getPhone());
            statement.setLong(5, patient.getId());
            statement.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void remove(Patient patient) throws SQLException {
        String sql = "DELETE FROM PATIENTS WHERE ID=?";
        try (Connection connection = getConnection();
             PreparedStatement statement = Objects.requireNonNull(connection).prepareStatement(sql)) {
            statement.setLong(1, patient.getId());
            statement.executeUpdate();
        }
    }
}

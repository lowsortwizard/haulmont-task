package com.haulmont.testtask.db;

import java.sql.*;

public class DatabaseHelper {

    private static final String CLASS_NAME = "org.hsqldb.jdbc.JDBCDriver";
    private static final String HOST = "jdbc:hsqldb:file:clinic";
    private static final String USER = "root";
    private static final String PASSWORD = "root";

    public static Connection getConnection() {
        try {
            Class.forName(CLASS_NAME);
            return DriverManager.getConnection(HOST, USER, PASSWORD);
        } catch (ClassNotFoundException | SQLException e) {
            e.printStackTrace();
            return null;
        }
    }
}


package com.haulmont.testtask;

import com.haulmont.testtask.data.DAOFactory;
import com.haulmont.testtask.data.Patient;
import com.haulmont.testtask.data.PatientsDAO;
import com.vaadin.data.Validator;
import com.vaadin.ui.*;
import com.vaadin.ui.themes.ValoTheme;

class SubPatientWindow extends Window {

    private static final int DEFAULT_ID = 0;
    private final DAOFactory factory = DAOFactory.getDaoFactory();
    private final PatientsDAO patientsDAO = factory.getPatientsDAO();

    SubPatientWindow(Patient patient) {
        super("Пациент");
        center();
        setClosable(false);
        setResizable(false);

        final VerticalLayout content = new VerticalLayout();
        content.setMargin(true);
        content.setSpacing(true);

        final TextField name = new TextField("Имя");
        name.addValidator(new TextFieldValidator(50));

        final TextField surname = new TextField("Фамилиия");
        surname.addValidator(new TextFieldValidator(50));

        final TextField patronymic = new TextField("Отчество");
        patronymic.addValidator(new TextFieldValidator(50));

        final TextField phone = new TextField("Номер телефона");
        phone.addValidator(new PhoneNumberValidator());

        final Button okButton = new Button("Ок");
        okButton.setStyleName(ValoTheme.BUTTON_PRIMARY);

        final Button cancelButton = new Button("Отменить");

        final HorizontalLayout buttons =
                new HorizontalLayout(okButton, cancelButton);
        buttons.setSpacing(true);

        content.addComponents(name, surname, patronymic,
                phone, buttons);

        name.setValue(patient.getName());
        surname.setValue(patient.getSurname());
        patronymic.setValue(patient.getPatronymic());
        phone.setValue(patient.getPhone());

        setContent(content);

        okButton.addClickListener(click -> {
            try {
                name.validate();
                surname.validate();
                patronymic.validate();
                phone.validate();

                patient.setName(name.getValue());
                patient.setSurname(surname.getValue());
                patient.setPatronymic(patronymic.getValue());
                patient.setPhone(phone.getValue());

                if (patient.getId() == DEFAULT_ID) {
                    patientsDAO.insert(patient);
                } else patientsDAO.update(patient);

                close();
            } catch (Validator.InvalidValueException e) {
                Notification.show("Проверьте корректность полей.");
            }
        });

        cancelButton.addClickListener(clickEvent -> close());
    }
}
